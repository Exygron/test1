#include "ugfx/gfx.h"
#include "ugfx/gdisp/image.h"
#include "ugfx/ginput/mouse.h"
#include "ugfx/gevent/gevent.h"
//---------------------------------------------------------------------------------
// Some constants
//---------------------------------------------------------------------------------
#define DEBUG_MODE				// we want to use debug mode
#define AUDIO_MODE					// we want audio

#define	DATA_BUF_QUEUE_SIZE	6		// number of messages in data buffer queue


#define CHAT_SAPI 			0x04    // sapi chat application number (0-7)
#define TIME_SAPI 			0x03    // sapi time application number (0-7)
#define BROADCAST_ADDRESS	0x0F	// broadcast address
#define TOKEN_TAG			0xFF	// tag of tokenring frame
#define TOKENSIZE			19		// size of a physical token frame
#define	MACTOKENSIZE		17		// size of a mac token frame
#define CONTROL_SRC_SIZE	1		// size of src in data frame in byte
#define CONTROL_DST_SIZE	1		// size of dst in data frame in byte
#define STATUS_SIZE			1		// size of status in data frame in byte
#define DATA_LENGTH_SIZE	1		// size in byte of data length
#define STX 				0x02	// token frame start char
#define ETX					0x03	// token frame end char
//---------------------------------------------------------------------------------
#define TX_RDY_EVT			0x01	// flag bit for RS232 TX rdy
#define BROADCAST_TIME_EVT	0x02	// flag bit of time to broadcast
//---------------------------------------------------------------------------------
// Type of messages
//---------------------------------------------------------------------------------
enum msgType_e
{
	TOUCH_EVENT,		///< touch has been pressed
	NEW_TOKEN,			///< a new token is requested
	START,				///< a start is requested (connected)
	STOP,				///< a stop is requested (disconnected)
	TOKEN_LIST,			///< token list sent to LCD
	MAC_ERROR,			///< error message to LCD
	TOKEN,				///< a token message
	DATA_IND,			///< a data sent between mac and app layers
	DATABACK,			///< a message is coming back
	TIME_MSG,			///< a time message is sent to LCD
	CHAR_MSG,			///< a single char is sent to the LCD
	CHAT_MSG,			///< a chat message is sent to the LCD
	FROM_PHY,			///< a message arriving from physical layer
	TO_PHY,				///< a message sent to physical layer
	TO_LOL				///< a test
};

//---------------------------------------------------------------------------------
// Current LCD view
//---------------------------------------------------------------------------------
enum currentView_e
{
	CONTROLS,			///< the Controls view is displayed
	COMMUNICATION,		///< the Communication view is displayed
	DEBUG				///< The Debug view is displayed
};
#define TAB_GROUP_MODE		0	// this will be tab group for mode
#define RADIO_GROUP_STATION		1	// group for radio buttons station
#define RADIO_GROUP_DEBUG	2	// debug mode choice

//---------------------------------------------------------------------------------
// The queue message structure
//---------------------------------------------------------------------------------
struct queueMsg_t
{
	enum msgType_e	type;	///< the type of message
	void * anyPtr;			///< the pointer to message (if any)
	uint8_t	addr;			///< the source or destination address
	uint8_t sapi;			///< the source or destination SAPI
};
//---------------------------------------------------------------------------------
// The interrupts ID trace and priorities
//---------------------------------------------------------------------------------

#define ID_LINE_ISR		2	// ID for Serial line ISR tracing
#define PRIO_LINE_ISR	11	// prioritiy of this ISR

#define ID_KB_ISR		3	// ID for Keyboard ISR tracing
#define PRIO_KB_ISR		13	// prioritiy of this ISR


//---------------------------------------------------------------------------------
// Global structure to reduce number of messages
//---------------------------------------------------------------------------------
struct TOKENINTERFACE
{
	uint8_t		myAddress;				///< my current address
	enum currentView_e	currentView;	///< the current view on LCD
	bool_t		debugOnline;			///< is debug station ON
	bool_t		connected;				///< are we connected
	bool_t		broadcastTime;			///< is broadcast time active
	bool_t		needReceiveCRCError;	///< did debug has to receive error
	bool_t		needSendCRCError;		///< didi debug has to send error
	uint32_t	debugSAPI;				///< current debug SAPI
	uint32_t	debugAddress;			///< current debug address
	bool_t		debugMsgToSend;			///< did debug have to send a message
	uint32_t	destinationAddress;		///< current destination address
	uint8_t		station_list[11];		///< 0 to 10 (0 is never used)
};
void TokenRingLine_Init(void);
void keyboard_isr_handler(void);
void UsartIsrHandler(void);
void usartSend(uint8_t data, uint8_t pos);

void CreateStationListButtons();
void UpdateStationListButtons();
void createWidgets(void);
void setTab(GHandle tab);

void DebugPhyFrame(uint8_t preChar,uint8_t * stringP);
void DebugMacFrame(uint8_t preChar,uint8_t * stringP);
